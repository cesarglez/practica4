#ifndef CONTROLESCOLAR_H
#define CONTROLESCOLAR_H
#include <iostream>
#include "Alumno.h"
using namespace std;

class ControlEscolar
{
      private:
              string Responsable;
              string Telefono;

      public:
			ControlEscolar();
            void Introduce_escolar();
            void Imprime_escolar(Alumno &X);
            // void ActualizaCarrera(Alumno &,string);
            // void ActualizaMatAprob(Alumno &, int);
            // void ActualizaProm(Alumno &, float);

};


#endif // CONTROLESCOLAR_H
