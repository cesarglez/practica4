#ifndef ALUMNO_H
#define ALUMNO_H
#include <iostream>
using namespace std;

class Alumno
{
      private:
              string nombreA,nombreC;
              int edad,matApro;
              float prom;
              friend class control_escolar;
      public:
			Alumno();
            void alta();
            void imprime_alumno();
 };

#endif // ALUMNO_H
